import moxios from 'moxios'
import CursoService from '../../../src/services/CursoService'

describe('Test all CRUD methods for curso', () => {
  let curso = null

  beforeEach(() => {
    curso = {
      id: 1,
      tipoCurso: 'Superior',
      modalidade: 'Mestrado',
      denominacaoCurso: 'Como passar em Projeto integrador 2 desenvolvimento',
      habilitacao: 'Grupo 3',
      localOferta: 'bloco b',
      turnoFuncionamento: 'matutino',
      numVagas: '200',
      cargaHora: 'modalidades para passar',
      regimeLetivo: 'Semestral',
      periodos: 'matutino e vespertino',
      professorName: 'Joao Pedro Lopez',
      cpf: '00011122233',
      highestDegree: 'Doutor',
      dedicatedTime: '65000'
    }
    moxios.install()
  })

  afterEach(() => {
    moxios.uninstall()
  })

  it('Should receive an array on getAll service', done => {
    let response = null
    CursoService.getAll().then((res) => {
      response = res.data
    })
    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 200,
        response: [curso]
      }).then(() => {
        expect(request.url).toBe('http://localhost:3001/api/cursos')
        expect(response).toEqual([curso])
        done()
      })
    })
  })

  it('Should append id on getById service ', done => {
    const id = 1
    let response = null
    CursoService.getById(id).then((res) => {
      response = res.data
    })
    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 200,
        response: curso
      }).then(() => {
        expect(request.url).toBe(`http://localhost:3001/api/cursos/${id}`)
        expect(response).toEqual(curso)
        done()
      })
    })
  })

  it('Should send body on save service', done => {
    let response = null
    CursoService.save(curso).then((res) => {
      response = res.data
    })
    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 201,
        response: request.config.data
      }).then(() => {
        expect(request.url).toBe('http://localhost:3001/api/cursos')
        expect(response).toEqual(curso)
        done()
      })
    })
  })

  it('Should send body on update service', done => {
    const id = 1
    let response = null
    CursoService.update(curso, id).then((res) => {
      response = res.data
    })
    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 201,
        response: request.config.data
      }).then(() => {
        expect(request.url).toBe(`http://localhost:3001/api/cursos/${id}`)
        expect(response).toEqual(curso)
        done()
      })
    })
  })
})
