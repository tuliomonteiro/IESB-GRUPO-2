import Api from '../../../src/services/Api'

describe('Test default API URL', () => {
  it('Should be localhost on port 3001', () => {
    const api = Api()
    expect(api.defaults.baseURL).toBe('http://localhost:3001/api/')
  })
})
