import moxios from 'moxios'
import BibliografiaService from '../../../src/services/BibliografiaService'

describe('Test all CRUD methods for bibliografia', () => {
  let bibliografia = null

  beforeEach(() => {
    bibliografia = {
      id: 1,
      curso: 1,
      disciplina: 1,
      titulo: 'Como passar em Projeto integrador',
      autor: 'Grupo 2',
      isbn: '00912028132',
      ano: 2018,
      editora: 'IESB'
    }
    moxios.install()
  })

  afterEach(() => {
    moxios.uninstall()
  })

  it('Should receive an array on getAll service', done => {
    let response = null
    BibliografiaService.getAll().then((res) => {
      response = res.data
    })
    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 200,
        response: [bibliografia]
      }).then(() => {
        expect(request.url).toBe('http://localhost:3001/api/bibliografia')
        expect(response).toEqual([bibliografia])
        done()
      })
    })
  })

  it('Should append id on getById service ', done => {
    const id = 1
    let response = null
    BibliografiaService.getById(id).then((res) => {
      response = res.data
    })
    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 200,
        response: bibliografia
      }).then(() => {
        expect(request.url).toBe(`http://localhost:3001/api/bibliografia/${id}`)
        expect(response).toEqual(bibliografia)
        done()
      })
    })
  })

  it('Should send body on save service', done => {
    let response = null
    BibliografiaService.save(bibliografia).then((res) => {
      response = res.data
    })
    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 201,
        response: request.config.data
      }).then(() => {
        expect(request.url).toBe('http://localhost:3001/api/bibliografia')
        expect(response).toEqual(bibliografia)
        done()
      })
    })
  })

  it('Should send body on update service', done => {
    const id = 1
    let response = null
    BibliografiaService.update(bibliografia, id).then((res) => {
      response = res.data
    })
    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 201,
        response: request.config.data
      }).then(() => {
        expect(request.url).toBe(`http://localhost:3001/api/bibliografia/${id}`)
        expect(response).toEqual(bibliografia)
        done()
      })
    })
  })
})
