import Api from '@/services/Api'

export default {
  save (professor) {
    return Api().post('professors', professor)
  },
  getById (id) {
    return Api().get('professors/' + id)
  },
  getAll () {
    return Api().get('professors')
  },
  update (professor, id) {
    return Api().put('professors/' + id, professor)
  }
}
