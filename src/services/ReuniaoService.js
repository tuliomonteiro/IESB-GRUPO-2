import Api from '@/services/Api'

export default {
  save (reuniao) {
    return Api().post('reuniao', reuniao)
  },
  getById (id) {
    return Api().get('reuniao/' + id)
  },
  getAll () {
    return Api().get('reuniao')
  },
  update (reuniao, id) {
    return Api().put('reuniao/' + id, reuniao)
  }
}
