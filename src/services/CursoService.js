import Api from './Api'

export default {
  save (curso) {
    return Api().post('cursos', curso)
  },

  getById (id) {
    return Api().get('cursos/' + id)
  },

  getAll () {
    return Api().get('cursos')
  },

  update (curso, id) {
    return Api().put('cursos/' + id, curso)
  }
}
