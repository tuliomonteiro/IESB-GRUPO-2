import Api from '@/services/Api'

export default {
  save (disciplina) {
    return Api().post('disciplina', disciplina)
  },

  getById (id) {
    return Api().get('disciplina/' + id)
  },

  getAll () {
    return Api().get('disciplina')
  },

  update (disciplina, id) {
    return Api().put('disciplina/' + id, disciplina)
  }
}
