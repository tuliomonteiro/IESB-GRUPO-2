import Api from '@/services/Api'

export default {
  save (ppc) {
    return Api().post('ppc', ppc)
  },
  getById (id) {
    return Api().get('ppc/' + id)
  },
  getAll () {
    return Api().get('ppc')
  },
  update (ppc, id) {
    return Api().put('ppc/' + id, ppc)
  }
}
