import Api from './Api'

export default {
  save (bibliografia) {
    return Api().post('bibliografia', bibliografia)
  },

  getById (id) {
    return Api().get('bibliografia/' + id)
  },

  getAll () {
    return Api().get('bibliografia')
  },

  update (bibliografia, id) {
    return Api().put('bibliografia/' + id, bibliografia)
  }
}
