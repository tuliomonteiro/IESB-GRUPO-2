import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import Dashboard from '@/components/Dashboard'
import SearchPPC from '@/components/ppc/SearchPPC'
import CadastroPPC from '@/components/ppc/CadastroPPC'
import CoursesForm from '@/components/courses/CoursesForm'
import ProfessorView from '@/components/professors/ProfessorView'
import DisciplinaForm from '@/components/disciplina/DisciplinaForm'
import BibliografiaForm from '@/components/bibliografia/BibliografiaForm'
import AtaReuniaoForm from '@/components/ataReuniao/AtaReuniaoForm'
import BootstrapVue from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard,
      children: [
        {
          path: 'cadastroPPC',
          name: 'cadastroPpc',
          component: CadastroPPC
        },
        {
          path: 'searchPPC',
          name: 'ppc',
          component: SearchPPC
        },
        {
          path: 'bibliografia',
          name: 'cadastroBibliografia',
          component: BibliografiaForm
        },
        {
          path: 'disciplina',
          name: 'cadastroDisciplina',
          component: DisciplinaForm
        },
        {
          path: 'addCourses',
          name: 'cadastrarCurso',
          component: CoursesForm
        },
        {
          path: 'professorView',
          name: 'professorView',
          component: ProfessorView
        },
        {
          path: 'ataReuniao',
          name: 'ataReuniaoElaborar',
          component: AtaReuniaoForm
        }
      ]
    }
  ]
})
