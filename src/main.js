// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
// import VueRest from 'vue-rest'

Vue.config.productionTip = false

// Vue.use(VueRest, {
//   axiosOptions: {
//     baseURL: 'http://localhost:3001/api'
//   }
// })

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
