# ppc-front

> Frontend para o projeto PPC - Projeto Integrador 3 - IESB - Grupo 2

## Evidências de Testes

[![pipeline status](https://gitlab.com/tuliomonteiro/IESB-GRUPO-2/badges/master/pipeline.svg)](https://gitlab.com/tuliomonteiro/IESB-GRUPO-2/commits/master)

[![coverage report](https://gitlab.com/tuliomonteiro/IESB-GRUPO-2/badges/master/coverage.svg)](https://gitlab.com/tuliomonteiro/IESB-GRUPO-2/commits/master)

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
